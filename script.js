"use strict"

const table = document.createElement('table');
for (let i = 0; i < 30; i++) {
  const row = document.createElement('tr');
  for (let j = 0; j < 30; j++) {
    const cell = document.createElement('td');
    row.appendChild(cell);
  }
  table.appendChild(row);
}

document.body.appendChild(table);

table.addEventListener('click', function(event) {
  if (event.target.nodeName === 'TD') {
    event.target.classList.toggle('black');
  }
});

document.body.addEventListener('click', function(event) {
  if (!table.contains(event.target)) {
    table.classList.toggle('invert');
  }
});
